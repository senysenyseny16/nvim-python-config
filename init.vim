
syntax on
set nocompatible

call plug#begin('~/.vim/plugged')

Plug 'preservim/nerdtree'

Plug 'Vimjas/vim-python-pep8-indent'

Plug 'brentyi/isort.vim'

Plug 'psf/black', { 'branch': 'stable' }

Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

Plug 'wookayin/fzf-ripgrep.vim'

Plug 'stsewd/fzf-checkout.vim'

Plug 'liuchengxu/vista.vim'

Plug 'jeetsukumaran/vim-pythonsense'

Plug 'jiangmiao/auto-pairs'

Plug 'neoclide/coc.nvim', {'branch': 'release'}

Plug 'Xuyuanp/nerdtree-git-plugin'

Plug 'tpope/vim-fugitive'

Plug 'lewis6991/gitsigns.nvim'

Plug 'kien/ctrlp.vim'

Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}

Plug 'ntpeters/vim-better-whitespace'

Plug 'mhinz/vim-startify'

Plug 'luochen1990/rainbow'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

Plug 'ryanoasis/vim-devicons'

Plug 'joshdick/onedark.vim'

Plug 'anntzer/vim-cython'

Plug 'kkoomen/vim-doge', { 'do': { -> doge#install() } }

call plug#end()

lua require('gitsigns').setup()

let doge_doc_standard_python = 'numpy'

" NERDTree
nnoremap <leader>n :NERDTreeFocus<CR>
nnoremap <C-n> :NERDTreeToggle<CR>
let NERDTreeQuitOnOpen = 1
let NERDTreeMinimalUI = 1
let NERDTreeDirArrowExpandable = "\u00a0"
let NERDTreeDirArrowCollapsible = "\u00a0"
let NERDTreeNodeDelimiter = "\x07"
let NERDTreeGitStatusWithFlags = 1

" isort
let g:isort_vim_options = join([
    \ '--force-single-line-imports',
	\ '--ensure-newline-before-comments',
	\ '--line-length 120',
	\ ], ' ')

" black
let g:black_linelength = 120
let g:black_skip_string_normalization = 1

" FZF
let $FZF_DEFAULT_OPTS = '--reverse'

" FZF-ripgrep
nnoremap <silent> <leader>f :RgFzf<cr>
nnoremap <silent> <leader>F :RgFzf ~<cr>

" FZF-Checkout
nnoremap <silent> <leader>gc :GCheckout<cr>

" CtrlP
if executable('rg')
  let g:ctrlp_user_command = 'rg %s --files --hidden --color=never --glob ""'
endif

" Coc
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

nnoremap <silent> K :call <SID>show_documentation()<CR>
function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Use <tab> for trigger completion and navigate to the next complete item
function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

inoremap <silent><expr> <Tab>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()

" Use <c-space> for trigger completion
inoremap <silent><expr> <c-space> coc#refresh()

inoremap <silent><expr> <cr> coc#pum#visible() && coc#pum#info()['index'] != -1 ? coc#pum#confirm() : "\<C-g>u\<CR>"
inoremap <expr> <Tab> coc#pum#visible() ? coc#pum#next(1) : "\<Tab>"
inoremap <expr> <S-Tab> coc#pum#visible() ? coc#pum#prev(1) : "\<S-Tab>"

" Fugitive
nmap <leader>gs :G<cr>
nmap <leader>gf :diffget //2<cr>
nmap <leader>gj :diffget //3<cr>

" Vista
let vista_default_executive = 'coc'
let vista_fzf_preview = ['right:50%']
let vista_sidebar_width = 45
let vista#renderer#enable_icon = 1
let vista#renderer#icons = {
\   "function": "\uf794",
\   "variable": "\uf71b",
\  }
nmap <F8> :Vista!!<CR>

" Airline
let airline#extensions#tabline#enabled = 1
let airline#extensions#tabline#fnamemod = ':t'
let airline#extensions#ale#enabled = 1
let airline_powerline_fonts = 1
let g:airline_theme='onedark'

" Startify
let startify_change_to_vcs_root = 1

colorscheme onedark

set nu

set fileformat=unix
set encoding=utf-8
set fileencoding=utf-8
set spelllang=en
nnoremap <silent> <F11> :set spell!<cr>
inoremap <silent> <F11> <C-O>:set spell!<cr>

set tabstop=4
set shiftwidth=4
set softtabstop=4
set colorcolumn=120
set expandtab
set noswapfile

" To treat all numerals as decimal
set nrformats=

" Enable mouse
set mouse=a

" Experimental
set updatetime=250

" Enable global statusline
set laststatus=3

" From documentation: If you want to keep the changed buffer without saving it, switch on the 'hidden' option.
set hidden

set nowrap
