# NeoVim configuration for Python :octopus:

- syntax highlighting
- autocompletion
- linting
- code static analysis
- symbol navigation

## Requirements

- [neovim](https://neovim.io/)
- [vim-plug](https://github.com/junegunn/vim-plug)
- [fzf](https://github.com/junegunn/fzf)
- [ripgrep](https://github.com/BurntSushi/ripgrep)
- [pylint](https://github.com/PyCQA/pylint)
- [nodejs](https://github.com/neoclide/coc.nvim) (for CoC)
- [nerdfonts](https://www.nerdfonts.com/)

## Getting started:

:hammer: Install all requirements

:wrench: Copy configuration files from repository to `~/.config/nvim`:
```
cd ~/.config/nvim
git clone https://gitlab.com/senysenyseny16/nvim-python-config.git .
```

:nut_and_bolt: Open `nvim` and run the following commands:
```
:PlugInstall
:CocInstall coc-pyright
```

Enjoy :shaved_ice:
